package facci.pm.ta3.sqlite.trabajoautonomo3sqlite.database.entities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import facci.pm.ta3.sqlite.trabajoautonomo3sqlite.database.helper.ShoppingElementHelper;
import facci.pm.ta3.sqlite.trabajoautonomo3sqlite.database.model.ShoppingItem;
import java.util.ArrayList;

public class ShoppingItemDB {

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";

    private ShoppingElementHelper dbHelper;

    public ShoppingItemDB(Context context) {
        // Create new helper
        dbHelper = new ShoppingElementHelper(context);
    }

    /* Inner class that defines the table contents */
    public static abstract class ShoppingElementEntry implements BaseColumns {
        public static final String TABLE_NAME = "entry";
        public static final String COLUMN_NAME_TITLE = "title";

        public static final String CREATE_TABLE = "CREATE TABLE " +
                TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT" + COMMA_SEP +
                COLUMN_NAME_TITLE + TEXT_TYPE + " )";

        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }


    public void insertElement(String productName) {
        //TODO: Todo el código necesario para INSERTAR un Item a la Base de datos
        SQLiteDatabase BasedeDatos = dbHelper.getWritableDatabase();
        /* De la clase SQLiteDataBase creamos un objeto que lo llamamos BaseDeDatos y con la ayuda del metodo
        getWritableDataBase de la clase dbHelper conseguimos abrir la base de datos en modo de escritura y lectura
         */

        ContentValues registro = new ContentValues();
        /*
        Creamos un objeto de la clase Content Values que le llamamos registro.
        La clase ContentValues sirve para retener los valores de un solo registro, que será el que se insertará.
         */
        registro.put(ShoppingElementEntry.COLUMN_NAME_TITLE, productName);

        /*
        el metodo put permite pasar los parametros, en este caso de la variable registro y
        lo pasa a la clave primaria(el nombre del producto) de la tabla de nuestra base de datos
         */

        BasedeDatos.insert(ShoppingElementEntry.TABLE_NAME, null,null);
        /*
        El metodo insert, permite insertar los valores a la tabla de nuestra base de datos
         */

        BasedeDatos.close();
        //Cierra la base de datos
    }


    public ArrayList<ShoppingItem> getAllItems() {

        ArrayList<ShoppingItem> shoppingItems = new ArrayList<>();

        String[] allColumns = { ShoppingElementEntry._ID,
            ShoppingElementEntry.COLUMN_NAME_TITLE};

        Cursor cursor = dbHelper.getReadableDatabase().query(
            ShoppingElementEntry.TABLE_NAME,    // The table to query
            allColumns,                         // The columns to return
            null,                               // The columns for the WHERE clause
            null,                               // The values for the WHERE clause
            null,                               // don't group the rows
            null,                               // don't filter by row groups
            null                                // The sort order
        );

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ShoppingItem shoppingItem = new ShoppingItem(getItemId(cursor), getItemName(cursor));
            shoppingItems.add(shoppingItem);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        dbHelper.getReadableDatabase().close();
        return shoppingItems;
    }

    private long getItemId(Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndexOrThrow(ShoppingElementEntry._ID));
    }

    private String getItemName(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndexOrThrow(ShoppingElementEntry.COLUMN_NAME_TITLE));
    }


    public void clearAllItems() {
        //TODO: Todo el código necesario para ELIMINAR todos los Items de la Base de datos
            SQLiteDatabase BaseDeDatos = dbHelper.getWritableDatabase();
            /*
            Crea el objeto BaseDeDatos de la clase SQLiteDataBase y abre la base de datos en modo de escritura.
             */
            BaseDeDatos.delete(ShoppingElementEntry.COLUMN_NAME_TITLE, null, null);
            /*
            Con el metodo delete borramos los valores de la base de datos en este caso las llaves primarias
            de la base de datos, por eso se borrarían todos los Items de la base de datos
            */

            BaseDeDatos.close();
            //Cerramos la base de datos

    }

    public void updateItem(ShoppingItem shoppingItem) {
        //TODO: Todo el código necesario para ACTUALIZAR un Item en la Base de datos
        SQLiteDatabase BaseDeDatos = dbHelper.getWritableDatabase();
        /*
        Crea el objeto BaseDeDatos de la clase SQLiteDataBase y abre la base de datos en modo de escritura.
        */

        String title = shoppingItem.getName();
        /*
        En esta variable de tipo String estará el nuevo valor
         */

        ContentValues actualizar = new ContentValues();
        /*
        creamos el objeto actualizar de la clase ContentValues, donde cogeremos el valor que vamos a actualizar
         */

        actualizar.put(ShoppingElementEntry.COLUMN_NAME_TITLE, title);
        /*
        Cogemos el valor que se guarda en la variable actualizar que recibe 2 parametros...
        el primero la clave primaria de la base de datos y la segunda es el valor de esa llave
        Cabe recalcar que tiene que coincidir la clave primaria para que se pueda actualizar los datos...
         */

        String selection = ShoppingElementEntry.COLUMN_NAME_TITLE + " LIKE ? ";
        String[] selectionArgs = {shoppingItem.getName()};
        /*
        Con estas líneas de código se está especificando que líneas de código se van
        a actualizar
         */

        BaseDeDatos.update(ShoppingElementEntry.TABLE_NAME, actualizar, selection, selectionArgs);
        /*
        Con esta línea de código se actualiza los valores de la base de datos
        El primer parametro es del nombre de la tabla a la que se le va actualizar el dato
        el segundo parametro, el de actualizar, es la clave primaria,
        el tercer y cuarto son los nuevos datos
         */

        BaseDeDatos.close();
        //Se cierra la base de datos


    }

    public void deleteItem(ShoppingItem shoppingItem) {
        //TODO: Todo el código necesario para ELIMINAR un Item de la Base de datos
        SQLiteDatabase BaseDeDatos = dbHelper.getWritableDatabase();
        /*
        Crea el objeto BaseDeDatos de la clase SQLiteDataBase y abre la base de datos en modo de escritura.
        */

        String selection = ShoppingElementEntry.COLUMN_NAME_TITLE + " LIKE ?";
        String [] selectionArgs = {shoppingItem.getName()};
        /*
        Selecciona la tupla que se va a borrar
         */

        BaseDeDatos.delete(ShoppingElementEntry.TABLE_NAME,selection,selectionArgs);
        // Se borra la tupla de la base de datos

        BaseDeDatos.close();
        //Se cierra la base de datos
    }
}
